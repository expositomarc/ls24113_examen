# Create host
host { 'localhost':
         ensure => 'present',
        target => '/etc/hosts',
        ip => '127.0.0.1',
        host_aliases => ['mysql1', 'memcached1']
}

# Miscellaneous packages.
$misc_packages = ['vim-enhanced','telnet','zip','unzip','git','screen','libssh2','libssh2-devel', 'gcc', 'gcc-c++', 'autoconf', 'automake']

package { $misc_packages: ensure => latest }

# Install apache
class{'apache':}

# Create virtual host
apache::vhost { 'mympwar.dev':
        port          => '80',
        docroot       => '/web',
}


# Install MYSQL and create bbdd
# Diu error <=,li ha passat a tothom! -->include '::mysql::server'
$databases = {
         'mympwar' => {
         ensure  => 'present',
         charset => 'utf8',
         },
}



# Install php 5.5
$yum_repo = 'remi-php55'
include ::yum::repo::remi_php55

class { 'php':
        version => 'latest',
        require => Yumrepo[$yum_repo]
}

php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap' ]: }

# Install memcached
include memcached

$nombre = "${operatingsystem}"
$version = "${operatingsystemrelease}"

# Create index and info
file {'/web/index.php':
        ensure => file,
        mode   => 0777,
        content => "Hello World. Sistema operativo <$nombre><$version>",
}

file {'/web/info.php':
        ensure  => file,
        mode => 0777,
        content => "<?php phpinfo(); ?>",
}


